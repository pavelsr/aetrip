import sys
# import requests
from requests_cache import CachedSession
from functools import lru_cache
import warnings
import logging

logger = logging.getLogger(__name__)

this = sys.modules[__name__]
this.session = CachedSession(
    cache_name=__name__+'.cache.sqlite',
    allowable_codes=[200, 400],
    # expire_after=360,
)

@lru_cache(maxsize=None)
def get_place_info(place_name):
    ''''''
    url = 'https://places.aviasales.ru/v2/places.json?locale=ru&types[]=city&types[]=airport&term=' + place_name
    logger.info('getting info for place with name=' + place_name)
    resp = this.session.get(url)
    if resp.status_code == 200:
        return resp.json()  # здесь есть также country_code и country_name
    else:
        resp.raise_for_status()


def get_country(name):
    '''
    Получает название страны по имени
    :param name:
    :return:
    '''

    x = get_place_info(name)

    possible_countries = [i['country_name'] for i in x]
    possible_countries = list(set(possible_countries))

    if len(possible_countries) > 1:
        # TODO: probably useless since &max=1 param set
        warnings.warn('More than one country found for city_name = ' + name + ' : ' + str(possible_countries))

    if x and 'country_name' in x[0]:
        return x[0]['country_name']
    else:
        warnings.warn('No country found for city_name = ' + name)
        return None



@lru_cache(maxsize=None)
def get_iata_code_by_city_name(name):
    '''
    API https://places.aviasales.ru/v2

    :param name: string название аэропорта/города
    :return: IATA код города

    ПРИМЕРЫ ИСПОЛЬЗОВАНИЯ
    x = aviasales.get_iata_code_by_city_name('Ростов-на-Дону')
    print(x) # ROV

    '''
    x = get_place_info(name)

    # TODO: city_code

    if len(x) > 1:
        # TODO: probably useless since &max=1 param set
        possible_codes = [ i['code'] for i in x ]
        warnings.warn('More than one IATA code found for city_name = ' + name + ' : ' + str(possible_codes) )

    if x and 'city_code' in x[0]:
        return x[0]['city_code']

    if x and 'code' in x[0]:
        return x[0]['code']
    else:
        warnings.warn('No IATA code found for city_name = ' + name)
        return None

# TODO: params validation
# TODO: fix check_integrity warning
def get_prices_on_month(origin_iata, destination_iata, depart_month=None, сheck_integrity=False):
    '''
    :param kwargs:
        origin_iata
        destination_iata
        depart_month
        сheck_integrity
        one_way = True (hard-coded)
    :return:

    {
    'prices': [
        {'depart_date': '2023-08-28', 'price': 1754},
        {'depart_date': '2023-08-29', 'price': 1754},
        {...},
        ...,
        ],
    'errors': []
    }

    Получает цены на месяц

    Пример использования:
        x = get_prices_on_month(depart_month="", origin_iata="MOW", destination_iata="LED")
        print(x)
    '''

    # kwargs['depart_month'] - первый день желаемого месяца в формате ISO 8601 (например 2023-08-01)
    # kwargs['destination_iata']

    # origin_iata
    # Пример запроса
    # https://lyssa.aviasales.ru/date_picker_prices?one_way=true&depart_month=2023-08-01&destination_iata=MOW&origin_iata=LED

    if not origin_iata:
        raise ValueError("No origin_iata provided")

    if not destination_iata:
        raise ValueError("No destination_iata provided")

    query_prms = {
        "one_way":          "true",
        "origin_iata":      origin_iata,
        "destination_iata": destination_iata,
    }

    if depart_month:
        query_prms['depart_month'] = depart_month

    resp = this.session.get('https://lyssa.aviasales.ru/date_picker_prices', params=query_prms)
    logger.info(str(query_prms))

    # LOGGER PROBLEM AttributeError: 'CachedSession' object has no attribute 'response'
    # logger.info(str(this.session.request.response.url))

    if resp.status_code == 200:
        x = resp.json()
        if 'prices' in x:
            if сheck_integrity:
                from aetrip.aeutils import days_in_month
                if (depart_month):
                    days_total = days_in_month(depart_month)
                    prices_total = len(x['prices'])
                    no_price_days_total = days_total-prices_total
                    warnings.warn('No prices for ' + str(no_price_days_total) + ' days in month')
                    return {
                        "prices": x['prices'],
                        "integrity": {
                            "prices_total": prices_total,
                            "days_total": days_total,
                            "no_price_days_total": no_price_days_total
                        }
                    }
            x['prices'] = sorted(x['prices'], key=lambda i: i['depart_date'], reverse=False)
            return x['prices']
        else:
            warnings.warn('No price data for params ' + str(query_prms))
            return []
    else:
        # TODO: Обработка не-200-х кодов

        # ПРИМЕРЫ:

        # HTTP 400 (origin_iata=ZIA)
        # https://lyssa.aviasales.ru/date_picker_prices?one_way=true&depart_month=2023-08-01&origin_iata=ZIA&destination_iata=SCO
        # {"errors": {"transform": "ZIA is not a city"}}

        # HTTP 400 (одинаковые origin_iata и destination_iata)
        # https://lyssa.aviasales.ru/date_picker_prices?one_way=true&depart_month=2023-08-01&origin_iata=NYM&destination_iata=NYM

        # не помню что было, но была ошибка
        # https: // lyssa.aviasales.ru/date_picker_prices?one_way=true&depart_month=2023-11-01&origin_iata=MRV&destination_iata=KGD
        # {'one_way': 'true', 'depart_month': '2023-11-01', 'origin_iata': 'MRV', 'destination_iata': 'KGD'}

        # resp.raise_for_status()
        try:
            warnings.warn(str(resp.json()))
        except Exception:
            warnings.warn(str(resp))
