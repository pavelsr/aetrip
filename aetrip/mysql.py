import sys
import mysql.connector

this = sys.modules[__name__]
this.db_con = None
this.is_db_initialized = False

'''
Модуль для работы сервиса с БД MySQL
'''

def init_con(conn_dict):
  '''
  Автоматически инициализует базу данных при первом вызове
  '''
  if this.db_con:
      return
  else:
      this.db_con = mysql.connector.connect(
        host= conn_dict["host"],
        user= conn_dict["user"],
        password = conn_dict["password"],
        database = conn_dict["database"],
      )
      if not this.is_db_initialized:
        _init_database()
        this.is_db_initialized = True
      return


def _init_database():
    c = this.db_con.cursor()
    # remove `id` INT NOT NULL AUTO_INCREMENT
    c.execute('''
         CREATE TABLE IF NOT EXISTS last_prices(
             `from_city` VARCHAR(255) NOT NULL,
             `from_iata` CHAR(3) NOT NULL,
             `from_country` VARCHAR(255),
             `to_city` VARCHAR(255) NOT NULL,
             `to_iata` CHAR(3) NOT NULL,
             `to_country` VARCHAR(255),
             `depart_date` DATE NOT NULL,
             `price` INT NOT NULL,
             CONSTRAINT PK_Price PRIMARY KEY (`from_iata`, `to_iata`, `depart_date`)
         );
     ''');
    c.execute('''
        CREATE TABLE IF NOT EXISTS prices_archive(
             `id` INT PRIMARY KEY AUTO_INCREMENT,
             `ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
             `from_city` VARCHAR(255) NOT NULL,
             `to_city` VARCHAR(255) NOT NULL,
             `from_iata` CHAR(3) NOT NULL,
             `to_iata` CHAR(3) NOT NULL,
             `depart_date` DATE NOT NULL,
             `price` INT NOT NULL
        );
    ''');
    # this.db_con.commit() # _mysql_connector.MySQLInterfaceError: Commands out of sync; you can't run this command now
    this.db_con.reconnect()
    # c.close()
    return

# def _dict_as_insert(payload_dict, table_name='last_prices'):
#     columns = ', '.join(payload_dict.keys())
#     placeholders = ', '.join(['%s'] * len(payload_dict))
#     sql = "INSERT INTO %s ( %s ) VALUES ( %s )" % (table_name, columns, placeholders)
#     return sql

def insert(payload_dict, table_name='last_prices'):
    query = 'INSERT INTO'
    if table_name == 'last_prices':
        query = 'REPLACE INTO'
    columns = ', '.join(payload_dict.keys())
    placeholders = ', '.join(['%s'] * len(payload_dict))
    query += ' %s (%s) VALUES (%s)' % (table_name, columns, placeholders)
    c = this.db_con.cursor()
    c.execute(query, list(payload_dict.values()))
    sql = c.statement
    print(sql)
    this.db_con.commit()
    return c.lastrowid

def insert_batch(AoD, table_name='last_prices'):
    '''
    https://dev.mysql.com/doc/connector-python/en/connector-python-api-mysqlcursor-executemany.html
    :param AoH_data:
    :param table_name:
    :return:
    '''
    # query must be like INSERT INTO table_name(feild_1,feild2,feild3) VALUES ( %(id)s, %(price)s, %(type)s)
    query = 'INSERT INTO'
    if table_name == 'last_prices':
        query = 'REPLACE INTO'

    fields = AoD[0].keys()
    columns_stmt_part = ', '.join(fields)
    values_stmt_part = ', '.join([ '%({})s'.format(x) for x in fields])

    query += ' %s (%s) VALUES (%s)' % (table_name, columns_stmt_part, values_stmt_part)
    print(query)
    c = this.db_con.cursor()
    c.executemany(query, AoD)
    sql = c.statement
    print(sql)
    this.db_con.commit()
    return c.lastrowid




# _mysql_connector.MySQLInterfaceError: Commands out of sync; you can't run this command now

# mysql.connector.errors.OperationalError: MySQL Connection not available.