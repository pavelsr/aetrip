import sys
import json
import sqlite3

this = sys.modules[__name__]
this.db_name = None
this.con = None
this.is_db_initialized = False

def _dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def _fix_sqlite_conn(con):
    '''
    returning "dictionary" rows after fetchall or fetchone.
    '''
    con.row_factory = _dict_factory
    return con

def init_con(database_name=this.db_name):
    '''
    Возвращает db connection (объект sqlite3.Connection)

    Автоматически инициализует базу данных при первом вызове
    '''
    if this.con:
        return this.con
    else:
        this.con = sqlite3.connect(database_name, check_same_thread=False)
        if not this.is_db_initialized:
            this.con = _fix_sqlite_conn(con)
            this.con = _init_database(con)
            this.is_db_initialized = True
        return this.con


# columns = [
#     'from',
#     'from_iata',
#     'to',
#     'to_iata',
#     'depart_date',
#     'price'
# ]
def _init_database(con=this.con):
    con.cursor().execute('''
       CREATE TABLE IF NOT EXISTS last_prices(
           `id` INTEGER PRIMARY KEY AUTOINCREMENT,
           `from_city` TEXT NOT NULL,
           `from_iata` TEXT NOT NULL,
           `to_city` TEXT NOT NULL,
           `to_iata` TEXT NOT NULL,
           `depart_date` TEXT NOT NULL,
           `price` INTEGER NOT NULL,
           UNIQUE(`from_iata`,`to_iata`,`depart_date`)
        );
    ''');
    con.cursor().execute('''
        CREATE TABLE IF NOT EXISTS prices_archive(
         `id` INTEGER PRIMARY KEY AUTOINCREMENT,
         `dt` TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
         `from_city` TEXT NOT NULL,
         `from_iata` TEXT NOT NULL,
         `to_city` TEXT NOT NULL,
         `to_iata` TEXT NOT NULL,
         `depart_date` TEXT NOT NULL,
         `price` INTEGER NOT NULL
        );
    ''');
    con.commit()
    return con

def check_keys(dictionary, key_list):
    for key in key_list:
        if (key not in dictionary) or (dictionary[key]==''):
            return False
    return True

def insert(payload_dict, table_name='last_prices', con=this.con):
    '''
    :param payload_dict:
    :param table_name:
    :param con:
    :return:  INSERT INTO last_prices ( depart_date, price, from_iata, to_iata ) VALUES ( %s, %s, %s, %s )
    '''
    print(payload_dict)
    required_params =  [
        'from_iata',
        'to_iata',
        'depart_date',
        'price'
    ]
    if(check_keys(payload_dict, required_params)):
        columns = ', '.join(payload_dict.keys())
        placeholders = ':'+', :'.join(payload_dict.keys())
        query = 'INSERT INTO'
        if table_name == 'last_prices':
            query = 'INSERT OR REPLACE INTO'
        query += ' %s (%s) VALUES (%s)' % (table_name, columns, placeholders)
        print(query)

        # TODO: handle closing by Ctrl+Z instead of Ctrl+C - fix sqlite3.OperationalError: database is locked
        # try:
        # ...
        # except KeyboardInterrupt:
        #   this.con.close()

        c = this.con.cursor()
        c.execute(query, payload_dict)
        this.con.commit()
        print('ROW', c.lastrowid)
        return c.lastrowid
    else:
        raise TypeError("Not all obligatory params provided for add_task")
