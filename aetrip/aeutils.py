from datetime import datetime
from calendar import monthrange
from dateutil.relativedelta import relativedelta

'''
Примеры использования

from aetrip import aeutils
x = { 'from_iata': 'LED', 'to_iata': 'MOW', 'depart_date': '2023-09-04' }
y = aeutils.get_aviasales_link(x)
print(y) # https://www.aviasales.ru/?params=LED0409MOW1&marker=228037.cheaptrip&filter_stops=true
y = aeutils.get_tinkoff_link(x) 
print(y) # https://www.tinkoff.ru/travel/flights/one-way/LED-MOW/04-09/

'''

def get_aviasales_link(payload_dict):
    # ?marker=228037.cheaptrip
    # Примеры ссылок
    # https://www.aviasales.ru/search/LED1203ROV14031 - туда-обратно
    # https://www.aviasales.ru/search/LED1203ROV1 - только туда
    # альтернатива (без поиска - https://www.aviasales.ru/?params=LED0409MOW1)
    '''
    Генерирует прямую ссылку на aviasales (с партнерским маркером)
    :param payload_dict:
    :return:
    '''
    if not payload_dict['from_iata'] or not payload_dict['to_iata'] or not payload_dict['depart_date']:
        return None

    # url = 'https://avs.io/search/'
    # url = 'https://aviasales.ru/search/'
    url = 'https://www.aviasales.ru/?params='
    dt = datetime.strptime(payload_dict['depart_date'], '%Y-%m-%d').date()
    url += payload_dict['from_iata']
    url += dt.strftime('%d')
    url += dt.strftime('%m')
    url += payload_dict['to_iata']
    url += '1'
    url += '&marker=228037.cheaptrip'
    url += '&filter_stops=true' # без пересадок
    return url

def get_tinkoff_link(payload_dict):
    # Примеры ссылок
    # https://www.tinkoff.ru/travel/flights/one-way/LED-MOW/09-05/
    '''
    Генерирует прямую ссылку на Тинькофф.Путешествия
    :param payload_dict:
    :return:
    '''
    if not payload_dict['from_iata'] or not payload_dict['to_iata'] or not payload_dict['depart_date']:
        return None

    url = 'https://www.tinkoff.ru/travel/flights/one-way/'
    dt = datetime.strptime(payload_dict['depart_date'], '%Y-%m-%d').date()
    url += '{from_iata}-{to_iata}/{day}-{month}/'.format(
        from_iata= payload_dict['from_iata'],
        to_iata= payload_dict['to_iata'],
        day= dt.strftime('%d'),
        month= dt.strftime('%m')
    )
    return url

def first_DoM_next_n_months(num_months_forward=0):
    '''
    Возвращает массив дат первых чисел месяца в ISO8601
    Используется для запроса к https://lyssa.aviasales.ru/date_picker_prices (aviasales.get_prices_on_month)

    :param num_months_forward: int (3), default = 0
    :return: list (e.g. ['2024-01-01', '2024-02-01', '2024-03-01'])
    '''
    if num_months_forward < 0:
        raise ValueError("wrong param: num_months_forward<0")
    now = datetime.now()
    first_day = datetime(now.year, now.month, 1)
    return [(first_day + relativedelta(months=months)).strftime('%Y-%m-%d') for months in range(num_months_forward+1)]

def days_in_month(first_date_of_month):
    '''
    Возвращает количество дней в месяце, месяц задается датой первого дня месяца
    :param first_date_of_month: string ('2024-01-01')
    :return: int (e.g. 31)
    '''
    supported_date_formats = ('%Y-%m-%d', '%Y-%m')
    for fmt in supported_date_formats:
        try:
            date_object = datetime.strptime(first_date_of_month, fmt).date()
            res = monthrange(date_object.year, date_object.month)[1]
            return res
        except ValueError:
            pass
    raise ValueError('date '+ first_date_of_month + ' has unsupported format, use '+str(supported_date_formats))
