import os
import sys
import copy
import logging
import warnings
from functools import lru_cache
from yaschedule.core import YaSchedule

import requests
# import requests_cache

# Install the cache for all requests functions by monkey-patching requests.Session
# install_cache() accepts all the same parameters as CachedSession:
# https://requests-cache.readthedocs.io/en/stable/modules/requests_cache.session.html#requests_cache.session.CachedSession

# requests_cache.install_cache('aetrip.yandex-rasp.cache') # BaseCache
# requests_cache.install_cache('aetrip.yandex-rasp.cache', expire_after=-1, ignored_parameters=('apikey'), allowable_codes=(200,404))

logger = logging.getLogger(__name__)

# TODO: print requests
# TODO: Поддержка нерусскоязычных аэропортов у get_yandex_city_code_by_name() - сменить функцию с get_all_country_airports на get_all_airports
# TODO: get_airports(country='Russia')
# TODO: добавить пример вывода функции
# TODO: Кэширование и загрузка stations list

### Гайдлайны
# избегать присваиваний stations_list (сделать переменную read-only)



this = sys.modules[__name__]
# this.yatoken = os.environ.get("AETRIP_YANDEX_TOKEN") or '366a6609-cf86-43a5-8e61-05c67c752ec4'
this.yatoken = os.environ.get("AETRIP_YANDEX_TOKEN")
if not this.yatoken:
    logger.warning('AETRIP_YANDEX_TOKEN or this.yatoken is not set, so data is available only from cache')

# this.yatoken = os.environ['AETRIP_YANDEX_TOKEN']

this.yaschedule = YaSchedule(this.yatoken)

if not hasattr(this.yaschedule, 'session'):
    import yaschedule
    from importlib.metadata import version
    warn_msg = "Seems like you are using old yaschedule version (<=0.0.4.0). "
    warn_msg += "Please run 'pip install yaschedule' if you want cache support "
    warn_msg += "Version used: " + version('yaschedule') + "\n"
    warn_msg += "PYTHONPATH: "+yaschedule.__file__
    # class YascheduleVersionException(Exception):
    #     pass
    # raise YascheduleVersionException('Wrong version of yaschedule, use > 0.0.5')

this.stations_list = this.yaschedule.get_all_stations()
this.debug = False
this.flight_map_build_progress = {
    "checked_codes": [],
    "unchecked_codes": [],
    "percentage": 0,
}

# print(len(this.stations_list))
# # warnings.warn((len(this.stations_list)))

def set_token(token):
    this.yatoken = token
    this.yaschedule = YaSchedule(token)
    return token

@lru_cache(maxsize=None)
def get_all_country_airports(country_name, stations_list=this.stations_list):
    '''
    :param stations_list: result of yaschedule.get_all_stations()
    :param country_name: title
    :return: list of country airports

    [
    ...,
        {
            'codes': {'yandex_code': 's9623388'},
            'title': 'Восточный',
            ...,
            'settlement': {'title': 'Курск', 'codes': {'yandex_code': 'c8'}},
            'region': {'title': 'Курская область', 'codes': {'yandex_code': 'r10705'}}
        }
    ]

    '''
    result = []
    country_regions_list = list(filter(lambda x : x['title'] == country_name, stations_list['countries']))[0]['regions']

    if not country_regions_list:
        warnings.warn('Country '+country_name+' not exist or has no regions')
        return None

    for region in country_regions_list:
        if 'settlements' in region and region['settlements']:
            for settlement in region['settlements']:
                if 'stations' in settlement and settlement['stations']:
                    for station in settlement['stations']:
                        if 'station_type' in station and station['station_type'] == 'airport':
                            x = copy.deepcopy(station)
                            del x['direction']        # always empty for airports (according docs)
                            del x['transport_type']   # always == plane
                            del x['station_type']     # always == airport
                            need_keys = ['title', 'codes']
                            x['settlement'] = { key: settlement[key] for key in need_keys }
                            x['region'] = { key: region[key] for key in need_keys }
                            result.append(x)
    return result

@lru_cache(maxsize=None)
def get_direct_flights_from_station(yandex_code, just_city_names=False, dryrun=False):
    '''
    Получает массив городов, в которые есть прямые авиарейсы из указанного города
    В качестве дополнительных метаданных в свойстве details есть:
        номер рейса
        код и название авиокомпании
        модель самолета
        даты в которых есть полеты
        время отправления
    :param yandex_code: может быть кодом как станции, так и кодом города
    :return: { to : , options: [ { thread.number, carrier, vehicle, uid, departure, days, except_days }... ]

    Return like
    {
        'from': {'name': 'Псков', 'yandex_code': 's9623414'},
        'to': {'name': 'Минск'},
        'options': [
                {
                    'thread': {...},
                    'terminal': None,
                    ...,
                    'arrival': None,
                    'days': '26\xa0августа, 2, 9, 16, 23\xa0сентября'
                },
                {
                    'thread': {...},
                    'terminal': None,
                    ...,
                    'arrival': None,
                    'days': '30\xa0августа, 6, 13, 20\xa0сентября'
                }
            ]
    }

    If just_city_names=True it will return simple list like
        ['City 1', 'City 2', ..., 'City X']

    '''
    if dryrun:
        url = 'https://api.rasp.yandex.net/v3.0/schedule/?apikey='+this.yatoken+'&lang=ru_RU&station='+yandex_code
        msg = 'yaschedule.get_station_schedule(' + yandex_code + ') : '+url
        print(msg)
        return

    schedule = this.yaschedule.get_station_schedule(station=yandex_code)

    # BUG1: Quickfix ошибки для города Минск (заметил этот баг впервые на нем)
    if not schedule or 'schedule' not in schedule:
        return None

    thread_names_list = list(map(lambda x:  x['thread']['title'], schedule['schedule']))
    unique_thread_names_list = list(set(thread_names_list))
    result = []
    for thread_title in unique_thread_names_list:
        # TODO: now using list-comprehension, try map
        options = [x for x in schedule['schedule'] if thread_title in x['thread']['title'] ]
        result.append({
            "from": {
                "name": schedule['station']['title'],
                "yandex_code": yandex_code,
            },
            "to": {
                "name": thread_title.split(' — ')[1]
            },
            "options":  options
        })

    if just_city_names:
        result = [ x['to']['name'] for x in result ]

    return result

def get_yandex_city_code_by_name(city_name):
    '''
    Предположительно работает только для всех российских городов
    (для Минска не срабатывает)
    :param city_name:
    :return:

    x = yandex_rasp.get_yandex_city_code_by_name('Ростов-на-Дону')
    print(x) # c39

    x = yandex_rasp.get_yandex_city_code_by_name('Минск')
    print(x) # None
    '''

    airports = get_all_country_airports('Россия')
    matched_cities = [ x for x in airports if city_name == x['settlement']['title'] ]

   #print(matched_cities)
    # import pprint
    # print(pprint(matched_cities))

    # https://stackoverflow.com/questions/25188119/test-if-code-is-executed-from-within-a-py-test-session
    if not matched_cities:
        if "pytest" not in sys.modules:
            warnings.warn('No yandex city_code found for city_name = ' + city_name)
        return None
    else:
        if len(matched_cities) > 1:
            codes = [ x['settlement']['codes']['yandex_code'] for x in matched_cities ]
            if "pytest" not in sys.modules:
                warnings.warn('More than one yandex_code found for city_name = ' + city_name + ': ' + str(codes))
                # from ppretty import ppretty
                # print(ppretty(matched_cities))
        return matched_cities[-1]['settlement']['codes']['yandex_code']


# TODO: KeyboardInterrupt handler - save progress (external file)
# TODO: lru_cache или аналогичное
# @lru_cache(maxsize=None)
def get_country_flight_map(
        country_name,
        dryrun = False,
        # use_json_cache=False,
        # save_json_cache=True,
        # json_cache_filename = 'flight_map_yandex.json'
    ):
    '''
    Получает карту полётов по России. Коды только яндекс, не IATA

    Вывод - массив массивов
    Первый уровень - города (одинаковое поле from)
    Второй уровень - рейсы

    По сути в цикле выполняет функцию get_direct_flights_from_station для каждого аэропорта
    И находит yandex_code для города назначения по имени

    Примерное время выполнения функции - полторы минуты, с кэшированием requests - около минуты, с lru_cache = 3-4 секунды

    return like
    [
        [ ... ] # city 1
        [       # city 2
            { ... }
            { 'from': {'name': 'Ханты-Мансийск', 'yandex_code': 's9623574'},
                'to': {'name': 'Минеральные Воды', 'yandex_code': 'c11063'}},
                'options': [ { 'arrival': None,
                     'days': '31\xa0августа, 7, 14, 21, 28\xa0сентября',
                     'departure': '17:05',
                     'except_days': None,
                     'is_fuzzy': False,
                     'platform': '',
                     'stops': '',
                     'terminal': None,
                     'thread': {
                        'carrier': { 'code': 29,
                                      'codes': { 'iata': 'UT',
                                                 'icao': 'UTA',
                                                 'sirena': 'ЮТ'},
                                      'title': 'ЮТэйр'},
                         'express_type': None,
                         'number': 'UT 468',
                         'short_title': 'Ханты-Мансийск — Минеральные Воды',
                         'title': 'Ханты-Мансийск — Минеральные Воды',
                         'transport_subtype': { 'code': None,
                                                'color': None,
                                                'title': None},
                         'transport_type': 'plane',
                         'uid': 'UT-468_230831_c29_12',
                         'vehicle': 'Boeing 737-400'}
                        }
                    ],
            }
        ]
    ]
    '''

    # if use_json_cache:
    #     import json
    #     f = open(json_cache_filename)
    #     flight_map=json.load(f)
    #     logger.info('loaded from ' + json_cache_filename)
    #     return flight_map

    flight_map = []

    airports = get_all_country_airports(country_name)
    total = len(airports)

    # TO-DO: 'parser' progress bar (start from)
    for idx, airport in enumerate(airports):
        station_code = airport['codes']['yandex_code']

        # ИДЕЯ: вместо аэропорта использовать город чтоб снизить количество запросов к API
        # (есть же города с несколькими аэропортами типа Москвы)
        # но! будет ошибка 'error': {'text': 'Не нашли станцию по yandex коду c25.' }
        # station_code = airport['settlement']['codes']['yandex_code'] # city
        if dryrun:
            flights = get_direct_flights_from_station(station_code, dryrun=True)
            continue

        # if this.debug:
        progress_str = str(idx) + '/' + str(total)
        idigits = (
            progress_str,
            station_code,
            airport['title'],
            airport['settlement']['title'],
            airport['region']['title']
        )
        msg = ":".join( idigits )
        logger.info('get_ru_flight_map:get_direct_flights_from_station:' + msg)

        flights = get_direct_flights_from_station(station_code)

        this.flight_map_build_progress['percentage'] = int(idx / total)
        this.flight_map_build_progress['checked_codes'].append(station_code)

        checked = this.flight_map_build_progress['checked_codes']
        all = [ x['codes']['yandex_code'] for x in airports ]
        unchecked = list(set(all) - set(checked))
        this.flight_map_build_progress['unchecked_codes'] = unchecked

        if flights is not None:  # Ошибка "Минск"
            for f in flights:
                f['to']['yandex_code'] = get_yandex_city_code_by_name(f['to']['name'])
            flight_map.append(flights)

    # if save_json_cache:
    #     import json
    #     with open(json_cache_filename, 'w') as f:
    #         json.dump(flight_map, f)

    return flight_map