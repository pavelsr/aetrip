import os
import sys
import copy
from aetrip import aviasales
from aetrip import yandex_rasp
from aetrip.aeutils import first_DoM_next_n_months

import logging
logger = logging.getLogger(__name__)

this = sys.modules[__name__]
this.dryrun = False
this.backend = None
this.prices_database_sqlite = None
this.progress = {}
this.mysql_conn_prms = {}
this.json_files = {
    'prices': None,
}
this.default_json_files = {
    'prices': 'prices.json'
}

'''
Основной модуль для запроса и получения данных
Параметры:
    backend: mysql | sqlite | json
    mysql_conn_prms (только для backend = mysql)
    prices_database (только для backend = sqlite)
    prices_json (только для backend = sqlite)
    
Пример настройки

import aedata
aedata.backend = 'mysql'

'''

# def get_county_by_city_iata:



def _db_insert(data, table_name=None, backend=this.backend, enrich_data = False):
    data_copy = copy.deepcopy(data)
    # if enrich_data:
    #     if 'from_city' in data_dict:
    #         data_dict['from_country'] = aviasales.get_country(data_dict['from_city'])
    #     if 'to_city' in data_dict:
    #         data_dict['to_country'] = aviasales.get_country(data_dict['to_city'])
    if this.backend == 'mysql':
        from aetrip import mysql as db
        # TODO: add support of dsn like 'mysql+pymysql://root:my-secret-pw@127.0.0.1/aetrip'
        mysql_defaults = {
            "host": "127.0.0.1",
            "user": "root",
            "password": "toor",
            "database": "aetrip",
        }
        this.mysql_conn_prms['host'] = os.environ.get("AETRIP_MYSQL_HOST") or mysql_defaults['host']
        this.mysql_conn_prms['user'] = os.environ.get("AETRIP_MYSQL_USER") or mysql_defaults['user']
        this.mysql_conn_prms['password'] = os.environ.get("AETRIP_MYSQL_PASSWORD") or mysql_defaults['password']
        this.mysql_conn_prms['database'] = os.environ.get("AETRIP_MYSQL_DATABASE") or mysql_defaults['database']
        # print(this.mysql_conn_prms)
        # mysql_conn_prms = this.mysql_conn_prms or mysql_defaults
        db.init_con(this.mysql_conn_prms)
        return db.insert_batch(data_copy, table_name)

    elif this.backend == 'sqlite':
        from aetrip import sqlite as db
        default_sqlite_database = 'aetrip.avia.prices.sqlite'
        database = this.prices_database_sqlite or default_sqlite_database
        db.init_con(database_name=database)
        return db.insert(data_dict, table_name)
        # this.mysql_dsn = 'mysql+pymysql://pavel:my-secret-pw@127.0.0.1/aetrip'

    else:
        raise ValueError("Unknown backend: "+backend)



# TODO: get IATA codes another way
# @lru_cache(maxsize=None)
def get_country_flight_map_iata(country_name, dryrun = False):
        # use_json_cache=False,
        # save_json_cache=True,
        # json_cache_filename = 'flight_map_iata.json'
    '''
    wrapper under yandex_rasp.get_ru_flight_map + aviasales.get_iata_code_by_city_name

    :return: AoA flight map
    AoA level1 - cities
    AoA level2 - routes

    {
      'from': {'name': 'Ханты-Мансийск', 'yandex_code': 's9623574', 'iata_code': 'HMA'},
      'to': {'name': 'Минеральные Воды', 'yandex_code': 'c11063', 'iata_code': 'MRV'},
      'options': []
    },
    '''

    # if use_json_cache:
    #     import json
    #     f = open(json_cache_filename)
    #     flight_map=json.load(f)
    #     logger.info('loaded from ' + json_cache_filename)
    #     return flight_map

    if dryrun:
        import inspect
        # __name__
        # print(inspect.stack()[1][3]) # will print <module>
        print(inspect.stack()[0][3])
        return

    flight_map = yandex_rasp.get_country_flight_map(country_name)

    total = len(flight_map)
    logger.info('got flight map from yandex with ' + str(total) + ' airports')

    for idx, city_arr in enumerate(flight_map):
        progress_str = str(idx) + '/' + str(total)
        logger.info('get_country_flight_map_iata:progress: '+progress_str)

        for route in city_arr:
            route['from']['iata_code'] = aviasales.get_iata_code_by_city_name(route['from']['name'])
            route['to']['iata_code'] = aviasales.get_iata_code_by_city_name(route['to']['name'])

    # if save_json_cache:
    #     import json
    #     with open(json_cache_filename, 'w') as f:
    #         json.dump(flight_map, f)

    return flight_map

def get_prices(
        routes,
        months_ahead = 0,
        expire_after = 600,
        save_data = False,
        track_progress = True,
        bothway = True
    ):
    '''
    :param routes: Массив хэшей с даннными маршрута
    Обычно в качестве этого параметра (routes) используется вывод метода get_country_flight_map_iata
    Но можно использовать и обычную структуру типа такой:
        {
        'from': {'name': 'Псков', 'yandex_code': 's9623414', 'iata_code': 'PKV'},
        'to': {'name': 'Москва', 'yandex_code': 'c213', 'iata_code': 'MOW'},
        'options': [
                {
                    'thread': {...},
                    'terminal': None,
                    ...,
                    'arrival': None,
                    'days': 'ежедневно с 29.10 по 27.03, кроме 01.01'
                },
                { ... }
            ]
        }
    Минимальный вход:
        { 'from': { 'iata_code': 'LED' }, 'to': { 'iata_code': 'MOW' } }


    :return:
    '''
    result = []

    expire_after_old = aviasales.session.settings.expire_after
    aviasales.session.settings.expire_after = expire_after

    # TODO: get prices from backend if expire_after is not left and data exists in cache
    # TODO: handle expire_after


    if not routes:
        raise ValueError("No routes provided for getting prices")

    # cursor = None
    # if save_data and this.backend in ('mysql', 'sqlite'):
    #     cursor = get_db_conn(this.backend)

    months = first_DoM_next_n_months(months_ahead)

    for idx_m, depart_month in enumerate(months):
        print(depart_month)
        for idx_r, route in enumerate(routes):

            if track_progress:
                progress_str = 'month '+str(idx_m+1) + '/' + str(len(months))
                progress_str += ' | '
                progress_str += 'route: '+str(idx_r+1) + '/' + str(len(routes))

                # if route['from']['name'] and route['to']['name']:
                if 'name' in route['from'] and 'name' in route['to']:
                    progress_str += ' ' + route['from']['name'] + '->' + route['to']['name']

                # if route['from']['iata_code'] and route['to']['iata_code']:
                #     progress_str += ' ' + route['from']['iata_code'] + '->' + route['to']['iata_code']

                logger.info('progress: ' + progress_str)

            if route['from']['iata_code'] and route['to']['iata_code']:
                prices = aviasales.get_prices_on_month(
                    depart_month=depart_month,
                    origin_iata=route['from']['iata_code'],
                    destination_iata=route['to']['iata_code'],
                )
                if prices:
                    for price in prices:
                        price['from_iata'] = route['from']['iata_code']
                        price['to_iata'] = route['to']['iata_code']
                        if 'name' in route['from']:
                            price['from_city'] = route['from']['name']
                        if 'name' in route['to']:
                            price['to_city'] = route['to']['name']

                if bothway:
                    prices_return = aviasales.get_prices_on_month(
                        depart_month=depart_month,
                        origin_iata=route['to']['iata_code'],
                        destination_iata=route['from']['iata_code'],
                    )
                    if prices_return:
                        for price in prices_return:
                            price['from_iata'] = route['to']['iata_code']
                            price['to_iata'] = route['from']['iata_code']
                            if 'name' in route['to']:
                                price['from_city'] = route['to']['name']
                            if 'name' in route['from']:
                                price['to_city'] = route['from']['name']
                        prices += prices_return

                if prices:
                    _db_insert(prices, table_name='last_prices', enrich_data=False)
                    _db_insert(prices, table_name='prices_archive')


                    for price in prices: # prices is AoH with elements like {'depart_date': '2023-08-28', 'price': 1754},
                    #     x = price

                        # x = {
                        #     'depart_date': price['depart_date'],
                        #     'price':       price['price'],
                        #     'from_iata':   route['from']['iata_code'],
                        #     'to_iata':     route['to']['iata_code'],
                        # }
                        #
                        # if 'name' in route['from']:
                        #     x['from_city'] = route['from']['name']
                        #
                        # if 'name' in route['to']:
                        #     x['to_city'] = route['to']['name']

                        # result.append(x)
                        result.append(price)

                        # if save_data and this.backend in ('mysql', 'sqlite'):
                        #     _db_insert(x, table_name='last_prices', enrich_data = True)
                        #     _db_insert(x, table_name='prices_archive')

    if save_data and this.backend == 'json':
        import json
        with open('prices.json', 'w') as f:
            json.dump(result, f)

    aviasales.session.settings.expire_after = expire_after_old

    return result
