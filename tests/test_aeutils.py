from aetrip import aeutils
from freezegun import freeze_time

@freeze_time("Jan 14th, 2024")
def test_first_DoM_next_n_months():
    assert aeutils.first_DoM_next_n_months(2) == ['2024-01-01', '2024-02-01', '2024-03-01']

    res = aeutils.first_DoM_next_n_months()
    assert res ==  ['2024-01-01']
    assert isinstance(res, list)

def test_days_in_month():
    assert aeutils.days_in_month('2024-01-01') == 31
    assert aeutils.days_in_month('2024-01') == 31
    assert aeutils.days_in_month('2024-02-01') == 29
    assert aeutils.days_in_month('2024-02') == 29

    res = aeutils.days_in_month('2022-02')
    assert res == 28
    assert isinstance(res, int)