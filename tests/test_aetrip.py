import aedata

from numpy import concatenate

def test_get_country_flight_map_iata():
    res = aedata.get_country_flight_map_iata('Россия')

    # alsmost same as in test for yandex_rasp.get_country_flight_map
    assert isinstance(res, list)
    assert isinstance(res[0], list)
    assert len(res) > 0
    assert any(x['from'] for x in res[0])
    assert any(x['to'] for x in res[0])
    assert any(x['options'] for x in res[0])
    assert any(len(x['options']) > 0 for x in res[0])

    assert any(x['from']['iata_code'] for x in res[0])
    assert any(x['to']['iata_code'] for x in res[0])


def test_get_prices():
    x = [{'from': {'iata_code': 'LED'}, 'to': {'iata_code': 'MOW'}}]
    res = aedata.get_prices(x)
    assert isinstance(res, list)
    assert any(x['depart_date'] for x in res)
    assert any(x['price'] for x in res)
    assert any(x['from_iata'] for x in res)
    assert any(x['to_iata'] for x in res)

    x = aedata.get_country_flight_map_iata('Россия')
    x = concatenate( x, axis=0 ).tolist()
    res = aedata.get_prices(x)
    assert isinstance(res, list)
    assert any(x['depart_date'] for x in res)
    assert any(x['price'] for x in res)
    assert any(x['from_iata'] for x in res)
    assert any(x['to_iata'] for x in res)

    x = [{'from': {'iata_code': 'LED', 'name': 'Петербург'}, 'to': {'iata_code': 'MOW', 'name': 'Москвабад'}}]
    aedata.backend='sqlite'
    res = aedata.get_prices(x, save_data=True)
    assert isinstance(res, list)
    assert any(x['depart_date'] for x in res)
    assert any(x['price'] for x in res)
    assert any(x['from_iata'] for x in res)
    assert any(x['to_iata'] for x in res)


