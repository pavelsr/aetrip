test_token = "12345678-abcd-1234-1234-abcdefghijkl"

import os
os.environ["AETRIP_YANDEX_TOKEN"] = test_token

from aetrip import yandex_rasp

def test_right_yaschedule_module_loading():
    # print(test_token)

    # TODO: FIX
    #  python3 -m pytest tests/test_yandex_rasp.py -> working
    #  python3 -m pytest -> NO :(
    # assert yandex_rasp.yatoken == test_token, 'token is loaded from AETRIP_YANDEX_TOKEN env'

    # check that stations_list is loaded fine and has structure same as in docs
    # https://yandex.ru/dev/rasp/doc/ru/reference/stations-list
    sl = yandex_rasp.stations_list
    assert 'countries' in sl
    assert 'regions' in sl['countries'][-1]
    assert 'settlements' in sl['countries'][-1]['regions'][-1]
    assert 'stations' in sl['countries'][-1]['regions'][-1]['settlements'][-1]


# will work only if linked yaschedule module version > 0.0.4
# TODO: check if yaschedule module version > 0.0.4 - cache is loaded from files
# def test_cache_defaults():
#     assert yandex_rasp.yaschedule.session.settings.cache_name == 'yaschedule.core.cache.sqlite3'
#     assert yandex_rasp.yaschedule.session.settings.allowable_codes == [200, 404]
#     assert yandex_rasp.yaschedule.session.settings.ignored_parameters == ['apikey']

def test_get_all_country_airports():
    res = yandex_rasp.get_all_country_airports('Россия')
    assert isinstance(res, list)
    assert len(res) > 0
    assert any(x['codes'] for x in res)
    assert any(x['codes']['yandex_code'] for x in res)
    assert any(x['title'] for x in res)

    assert any(x['settlement'] for x in res)
    assert any(x['settlement']['title'] for x in res)
    assert any(x['settlement']['codes']['yandex_code'] for x in res)

    assert any(x['region'] for x in res)
    assert any(x['region']['title'] for x in res)
    assert any(x['region']['codes']['yandex_code'] for x in res)

def test_get_yandex_city_code_by_name():
    assert yandex_rasp.get_yandex_city_code_by_name('Москва') == 'c213'
    assert yandex_rasp.get_yandex_city_code_by_name('Санкт-Петербург') == 'c2'
    assert yandex_rasp.get_yandex_city_code_by_name('Ростов-на-Дону') == 'c39'

def test_get_country_flight_map():
    res = yandex_rasp.get_country_flight_map('Россия')
    assert isinstance(res, list)
    assert isinstance(res[0], list)
    assert len(res) > 0

    assert any(x['from'] for x in res[0])
    assert any(x['to'] for x in res[0])
    assert any(x['options'] for x in res[0])
    assert any(len(x['options']) > 0 for x in res[0])

# will work only if linked yaschedule module version > 0.0.4
def test_get_direct_flights_from_station():
    res = yandex_rasp.get_direct_flights_from_station('s9623414')
    print(res)
    assert isinstance(res, list)
    assert len(res) > 0
    assert any(x['from'] for x in res)
    assert any(x['to'] for x in res)
    assert any(x['options'] for x in res)
    assert any(len(x['options']) > 0 for x in res)
