from aetrip import aviasales
from datetime import date

def test_get_iata_code_by_city_name():
   res = aviasales.get_iata_code_by_city_name('Ростов-на-Дону')
   assert res == 'ROV'

def test_get_prices_on_month():
   res = aviasales.get_prices_on_month(origin_iata='LED',destination_iata='MOW')
   assert isinstance(res, list)
   assert len(res) > 0, "aviasales.get_prices_on_month(depart_month=None) result is non empty list"
   assert any(x['depart_date'] for x in res)
   assert any(x['price'] > 0 for x in res)

   test_depart_month = date.today().replace(day=1).strftime('%Y-%m-%d')
   res = aviasales.get_prices_on_month(
      origin_iata='LED',
      destination_iata='MOW',
      depart_month=test_depart_month
   )
   assert isinstance(res, list)
   assert len(res) > 0, "aviasales.get_prices_on_month(depart_month=<this_month>) result is non empty list"
   assert any(x['depart_date'] for x in res)
   assert any(x['price'] > 0 for x in res)

   res = aviasales.get_prices_on_month(
      origin_iata='LED',
      destination_iata='MOW',
      depart_month=test_depart_month,
      сheck_integrity=True
   )
   assert isinstance(res["prices"], list)
   assert len(res["prices"]) > 0, "aviasales.get_prices_on_month(depart_month=<this_month>) result is non empty list"
   assert any(x['depart_date'] for x in res["prices"])
   assert any(x['price'] > 0 for x in res["prices"])
   assert 'integrity' in res
   assert 'prices_total' in res['integrity']
   assert 'days_total' in res['integrity']
   assert res['integrity']['prices_total'] > 0
   assert res['integrity']['days_total'] > 0
   assert 'no_price_days_total' in res['integrity']