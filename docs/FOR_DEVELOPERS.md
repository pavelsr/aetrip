# Поведение кэшей

## aetrip.yandex_rasp

/stations_list - самый тяжеловесный модуль API, загружает файл около 40Мб

Поэтому он сразу загружается в память при импорте модуля `aetrip.yandex_rasp`

Такая архитектура выбрана т.к. данные этого файла используют 3 из 4 функций модуля (кроме get_direct_flights_from_station): 

get_all_country_airports,
get_yandex_city_code_by_name (использует get_direct_flights_from_station),
get_ru_flight_map (использует get_yandex_city_code_by_name)

Наглядно

```commandline
echo -e "from aetrip import yandex_rasp" | python3  
# API requests qty=1
```

```commandline
echo -e "from aetrip import yandex_rasp as y\nprint(y.get_all_country_airports('Россия'))\nprint(y.get_yandex_city_code_by_name('Ростов-на-Дону'))" | python3
# also API requests qty=1
```

Если используется новая версия модуля yaschedule то поддержка кеша включается автоматически

В папке выполнения скрипта автоматически создается файл yaschedule.core.cache.sqlite3 после первого же запроса к API




Если использовать requests_cache.install_cache вместо CachedSession может возникнуть

`sqlite3.OperationalError: database is locked`

Если делать Insert по одному - скрипт работает 180 минут. А если разом по одному направлению - по 1 минуте


(42000): Incorrect table definition; there can be only one auto column and it must be defined as a key