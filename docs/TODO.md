✅ 1) insert_batch instead of insert (via cursor.executemany)

=======

2) Названия городов вместо аэропортов (Жуковский, Грабцево, Бегишево)
С городами у которых несколько аэропортов нужно подумать чё делать отдельно

=======

3) добавить aetool -с возможность получить параметры из yaschedule.session
https://stackoverflow.com/questions/71769975/in-the-python-requests-cache-package-how-do-i-detect-a-cache-hit-or-miss

=======

4) БАГ
Запросы к API Яндекс.Расписаний не обращают внимание на кеш
Больше других файлов нет
yaschedule.core.cache
yaschedule.core.cache.sqlite

=======

5) Добавить в yaschedule 
print(response.from_cache, response.created_at, response.expires, response.is_expired)
False None None None
https://requests-cache.readthedocs.io/en/stable/user_guide/inspection.html#response-details

TO-DO: сделать вывод предполагаемого url в dry-run чтоб потом проверить если ли он в кэше (через session.cache.contains(url=)
https://api.rasp.yandex.net/v3.0/schedule/?apikey=e0ec446d-401d-4881-ade8-6f63796abbcf&lang=ru_RU&station=s9830078

?apikey=REDACTED

dry-run implemented:
✅ yandex_rasp.get_direct_flights_from_station
✅ yandex_rasp.get_country_flight_map
✅ aedata.get_country_flight_map_iata

=======

✅ aedata.get_prices - two-way

Предупреждение когда запросов к Yandex API близко к 50 (yaschedule)

--dry-run option - калькуляция количества INSERT запросов к базе и запросов к API и количества вставленных строк прежде чем их делать

✅ MYSQL && SQlite UNIQUE() (INSERT OR REPLACE)

✅ табличка с архивом цен (будет целесобразна если скрипт будет запускаться как минимум раз в час)

✅❌ Обработчик Ctrl+C/Ctrl+Z - закрытие БД sqlite

Запросы к БД параллельно. 
Идеи: общий кеш requests_cache, API с заданиями, в результате которой создаётся маленькая БД sqlite, а затем 
она по завершению мерджится в общую базу

УСКОРЕНИЕ ПОЛУЧЕНИЯ ДАННЫХ В БД:

Запросы к БД пакетно
Разбить на разные воркеры - например, по месяцу или стране
tune max_allowed_packet


❌ loading envs via aetool
