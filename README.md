# Что это?

Полностью переписанный на Python проект russia.raw.aetrip.ru

Теперь модуль умеет не только russia

# Установка

```commandline
pip3 install -r requirements.txt
```

# Запустить тесты

```commandline
python3 -m pytest
```

Если вы хотите запустить тесты с измененной версией `yaschedule` - 
не забудьте в сессии тестов установить соответствующий PYTHONPATH,
например `PYTHONPATH="/home/a/projects/yaschedule:${PYTHONPATH}"`


# Используемые переменные окружения

```commandline
AETRIP_YANDEX_TOKEN # токен для доступа к api Яндекс.Расписаний
AETRIP_MYSQL_HOST
AETRIP_MYSQL_USER
AETRIP_MYSQL_PASSWORD
AETRIP_MYSQL_DATABASE
```

# Примеры

```commandline
# cp .env.example .env
# export $(grep -v '^#' .env | xargs)
python3 bin/aetool -a get_prices -b mysql -v
python3 -m pytest --disable-pytest-warnings
```

Вместо `$(grep -v '^#' .env | xargs)` вы можете использовать:

```commandline
set -o allexport
source .env
set +o allexport
```


# Репозиторий

```commandline
git remote add origin git@gitlab.com:pavelsr/aetri.git
```