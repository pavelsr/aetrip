#!/usr/bin/env python3
import os
import argparse

parser = argparse.ArgumentParser(description='CMD tool for aetrip.ru project')

parser.add_argument('-a', '--action',
    help='Нужное действие')

parser.add_argument('-b', '--backend',
    default='sqlite',
    help='Куда сохранять цены')

parser.add_argument('-c', '--config',
    action='store_true',
    help='Загружает переменные окружения из файла .env и показывает текущий конфиг')

parser.add_argument('-d', '--dryrun',
    action='store_true',
    help='Показывает сколько запросов к API будет сделано, не выполняет их')

parser.add_argument('-m', '--months',
    type=int,
    default=3,
    help='Устанавливает на сколько месяцев вперед получать цены за один проход')

parser.add_argument('-v', '--verbose',
    action='store_true',
    help='Больше промежуточного вывода')

args = parser.parse_args()
print(args)
print (os.getpid())

import logging
logger = logging.getLogger(__name__)
import warnings

if args.config:
    print("aetrip.ru ENV variables of current user/session:")
    from dotenv import dotenv_values
    config = dotenv_values(".env.example")
    for e in config.keys():
        print(e + "="+str(os.getenv(e)))
    import yaschedule
    print("yaschedule PATH: " + yaschedule.__file__)
    from importlib.metadata import version
    print("yaschedule version used: " + version('yaschedule'))
    from yaschedule.core import YaSchedule
    ys= YaSchedule('thisistoken')
    # https://requests-cache.readthedocs.io/en/stable/user_guide/inspection.html#response-details
    # print(ys.session)
    # print(ys.session.settings.expire_after)
    # print(ys.session.cache_name)
    # print(ys.session.cache)
    # print(ys.session.cache.contains(url='https://api.rasp.yandex.net/'))
    # print(ys.session.cache.urls())
    # print(ys.session.cache.responses.values())
    # print(len(ys.session.cache.urls()))
    # print(type(ys.session.cache.filter(expired=False)))

    # print(ys.session.settings)

    cache_info = {
        "defaults": {
            a : getattr(ys.session.settings, a) for a in dir(ys.session.settings) if not a.startswith('_')
        },
        "urls": len(ys.session.cache.urls())
    }
    cache_info['defaults'].pop('from_kwargs', None)
    # from ppretty import ppretty
    # print(ppretty(cache_info))

    import pprint
    pp = pprint.PrettyPrinter(indent=2)
    print('yaschedule cache info : ')
    pp.pprint(cache_info)

    test_url = 'https://api.rasp.yandex.net/v3.0/schedule/?apikey=e0ec446d-401d-4881-ade8-6f63796abbcf&lang=ru_RU&station=s9623388'
    print(ys.session.cache.contains(url=test_url))
    # x = ys.session.cache.filter(expired=False)
    # print(len(x))

    for response in ys.session.cache.filter():
        print(response)

    # keys = [response.cache_key for response in expired_responses]



if args.verbose:
    logging.basicConfig(level='INFO')
    logger = logging.getLogger(__name__)
    # logger.info(yaschedule.__file__)
    logging.getLogger('aetrip.aviasales').setLevel(logging.INFO)
    logging.getLogger('aetrip.yandex_rasp').setLevel(logging.INFO)
    logging.getLogger('urllib3').setLevel(logging.DEBUG)  # instead of 'requests.packages.urllib3'
    warnings.filterwarnings("ignore")

# if args.backend:
#     # Тестирует бэкенд (если он mysql)

if args.action == 'get_prices':

    if args.dryrun:
        print('If you want to run "./aetool.py -a get_prices -b mysql" in dryrun mode just simply run ./aetool.py -d')
        exit(0)

    # Средние времена выполнения:
    # Целиком из кеша months = 3 (полезно при тестах) - (вставляет около 124k строк)
    # Наживую из aviasales months = 3 -
    import aedata
    import numpy
    map = aedata.get_country_flight_map_iata('Россия', dryrun=args.dryrun) # ~ 10s
    all_routes = numpy.concatenate( map, axis=0 ).tolist() # 2d -> 1d array
    aedata.backend = args.backend
    if not aedata.backend:
        logger.warning('backend is not set, so data only will be printed on screen without saving anywhere')
    logger.info('Start getting prices data (backend = ' + aedata.backend + ')...')
    prices = aedata.get_prices(all_routes, save_data=True, months_ahead=args.months)
    if args.verbose:
        print(prices)


if args.dryrun:
    import aedata
    from aetrip import yandex_rasp
    aedata.dryrun = True
    # etrip.yandex_rasp:get_ru_flight_map:get_direct_flights_from_station
    # map = aedata.get_country_flight_map_iata('Россия')
    map = yandex_rasp.get_country_flight_map('Россия', dryrun=True)
    print(map)