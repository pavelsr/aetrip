# Скрипт для генерации build.json из prices.json
# https://mottie.github.io/tablesorter/docs/example-widget-build-table.html


import json
f = open('prices.json')
prices=json.load(f)
print(prices[-1])

columns = [
    'from',
    'from_iata',
    'to',
    'to_iata',
    'depart_date',
    'price'
]

result = {
    'headers':[[]],
    'footers': 'clone',
    'rows':[]
}
for column_name in columns:
    result['headers'][0].append(
        # { "text": column_name }
        column_name
    )
# for price_dict in prices:
#     row = []
#     for column_name in columns:
#         row.append(price_dict[column_name])
#     result['rows'].append(row)

row = []
for column_name in columns:
    row.append(prices[-1][column_name])
    result['rows'].append(row)

with open('build.json', 'w') as f:
    json.dump(result, f)

