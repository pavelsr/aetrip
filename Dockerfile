# docker build -t pavelsr/aetrip-cron .

# test
# docker run -it -v $(pwd):/app --env-file .env pavelsr/aetrip-cron
# echo $AETRIP_YANDEX_TOKEN
# ./aetool.py -a get_prices -b mysql -m 0 -v

# docker run -it -v $(pwd):/app --env-file .env pavelsr/aetrip-cron ./aetool.py -a get_prices -b mysql -m 0 -v


# TO-DO: make module as distro

FROM python:3.9.18-alpine3.18

RUN apk update && \
    apk add git

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip install --upgrade pip && \
		pip install -r requirements.txt

RUN	rm -f requirements.txt

CMD sh
# ENTRYPOINT ["sh"]
# SHELL ["/bin/sh", "-c"]
